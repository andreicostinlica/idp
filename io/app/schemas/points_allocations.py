from app import mm
from marshmallow import fields
from marshmallow.validate import Length


class PointsAllocationsSchema(mm.SQLAlchemyAutoSchema):
    id                = fields.Integer(primary_key=True)
    result_type       = fields.String(required=True, validate=Length(max=20))
    points_for_result = fields.Integer(required=True)
