from app import mm
from marshmallow import fields


class PredictionsSchema(mm.SQLAlchemyAutoSchema):
    id       = fields.Integer(primary_key=True)
    match_id = fields.Integer(required=True)
    score1   = fields.Integer(required=True)
    score2   = fields.Integer(required=True)
    user_id  = fields.Integer(required=True)