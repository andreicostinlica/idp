from app import app, db
from flask import request
from app.responses.responses import *
from app.models.points_allocations import PointsAllocationsModel
from app.schemas.points_allocations import PointsAllocationsSchema


# Serialization and validation
points_allocations_schema = PointsAllocationsSchema()

# POST /points
@app.route("/points", methods=["POST"])
def add_points_allocation():
    data = request.get_json(silent=True)
    errors = points_allocations_schema.validate(data)

    # Validate body
    if errors or not data:
        return bad_request()
    
    # Verify new object satisfies unique constraint
    points_allocation = PointsAllocationsModel.query.filter_by(result_type=data['result_type']).first()
    if points_allocation:
        return conflict()

    # Create new object
    points_allocation = PointsAllocationsModel(result_type=data['result_type'], \
                                               points_for_result=data['points_for_result'])
    db.session.add(points_allocation)
    db.session.commit()

    # Return the newly created object
    return created(points_allocations_schema.jsonify(points_allocation))


# GET /points
@app.route("/points", methods=["GET"])
def get_points_allocations():
    # Get all objects
    points_allocations = PointsAllocationsModel.query.all()

    # Return all objects
    return ok(points_allocations_schema.jsonify(points_allocations, many=True))


# PUT /points/:id
@app.route("/points/<int:id>", methods=["PUT"])
def update_points_allocation(id):
    data = request.get_json(silent=True)
    errors = points_allocations_schema.validate(data)
    # Validate body
    if errors or not data:
        return bad_request()
    
    # Verify object to be updated exists
    points_allocation = PointsAllocationsModel.query.get(id)
    if not points_allocation:
        return not_found()

    # Update object
    points_allocation.result_type = data['result_type']
    points_allocation.points_for_result = data['points_for_result']

    db.session.commit()

    return ok()


# DELETE /points/:id
@app.route("/points/<int:id>", methods=["DELETE"])
def delete_points_allocation(id):
    # Get object
    points_allocation = PointsAllocationsModel.query.get(id)

    # Verify object exists
    if not points_allocation:
        return not_found()
    
    # Delete object
    db.session.delete(points_allocation)
    db.session.commit()

    return ok()
