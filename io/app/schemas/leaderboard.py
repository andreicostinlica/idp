from app import mm
from marshmallow import fields


class LeaderboardSchema(mm.SQLAlchemyAutoSchema):
    id                = fields.Integer(primary_key=True)
    user_id           = fields.Integer(required=True)
    aprox_results     = fields.Integer()
    correct_results   = fields.Integer()
    incorrect_results = fields.Integer()
    total_points      = fields.Integer()

    class Meta:
        # Include foreign key fields
        include_fk = True