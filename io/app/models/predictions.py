from app import db


class PredictionsModel(db.Model):
    __tablename__ = "predictions"
    id       = db.Column(db.Integer, primary_key=True, autoincrement=True)
    match_id = db.Column(db.Integer)
    score1   = db.Column(db.Integer)
    score2   = db.Column(db.Integer)
    user_id  = db.Column(db.Integer, nullable=False)
