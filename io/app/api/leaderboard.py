from app import app, db
from flask import request
from app.responses.responses import *
from app.models.leaderboard import LeaderboardModel
from app.schemas.leaderboard import LeaderboardSchema


# Serialization and validation
leaderboard_schema = LeaderboardSchema()

# POST /leaderboard
@app.route("/leaderboard", methods=["POST"])
def add_leaderboard():
    data = request.get_json(silent=True)
    errors = leaderboard_schema.validate(data)
    # Validate body
    if errors or not data:
        return bad_request()
    
    # Verify new object satisfies unique constraint
    leaderboard = LeaderboardModel.query.filter_by(user_id=data['user_id']).first()
    if leaderboard:
        return conflict()

    # Create new object
    leaderboard = LeaderboardModel(user_id=data['user_id'], \
                                   aprox_results=data['aprox_results'], \
                                   correct_results=data['correct_results'], \
                                   incorrect_results=data['incorrect_results'], \
                                   total_points=data['total_points'])
    db.session.add(leaderboard)
    db.session.commit()

    # Return the newly created object
    return created(leaderboard_schema.jsonify(leaderboard))

# GET /leaderboard
@app.route("/leaderboard", methods=["GET"])
def get_leaderboard():
    # # Get all objects
    leaderboard = LeaderboardModel.query.all()

    # # Return all objects
    return ok(leaderboard_schema.jsonify(leaderboard, many=True))

# GET /leaderboard/user/id
@app.route("/leaderboard/<id>", methods=["GET"])
def get_by_user_id(id):
    # Get leaderboard user by id
    leaderboard_user = LeaderboardModel.query.filter_by(user_id=id).first()

    return ok(leaderboard_schema.jsonify(leaderboard_user))

# PUT /leaderboard/:id
@app.route("/leaderboard/<id>", methods=["PUT"])
def update_leaderboard(id):
    print(request)
    print(request.headers)
    print(request.data)
    data = request.get_json()
    print(data)
    errors = leaderboard_schema.validate(data)
    print(errors)
    # Validate body
    if errors or not data:
        return bad_request()
    
    # Verify object to be updated exists
    leaderboard = LeaderboardModel.query.get(id)
    if not leaderboard:
        return not_found()

    # Update object
    leaderboard.user_id = data['user_id']
    leaderboard.aprox_results = data['aprox_results']
    leaderboard.correct_results = data['correct_results']
    leaderboard.incorrect_results = data['incorrect_results']
    leaderboard.total_points = data['total_points']

    db.session.commit()

    return ok()


# DELETE /leaderboard/:id
@app.route("/leaderboard/<int:id>", methods=["DELETE"])
def delete_leaderboard(id):
    # Get object
    leaderboard = LeaderboardModel.query.get(id)

    # Verify object exists
    if not leaderboard:
        return not_found()
    
    # Delete object
    db.session.delete(leaderboard)
    db.session.commit()

    return ok()
